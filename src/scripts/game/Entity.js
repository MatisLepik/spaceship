/**
 * Entity is an interface the provides the basic functions that any entity is supposed to have.
 */

export default class Entity {

  constructor() {
    this.posX = this.posY = this.speedX = this.speedY = this.rotation = 0;
    this.isUsed = false;
  }

  tick(delta) {
    if (!this.isUsed) return;
    this.draw();
    this.physx(delta);
  }

  activate() {
    console.error(`Non-overridden call to activate found. This class implements Entity and should override this method.`);
  }

  draw() {
    console.error(`Non-overridden call to draw found. This class implements Entity and should override this method.`);
  }

  physx() {
    console.error(`Non-overridden call to physx found. This class implements Entity and should override this method.`);
  }
}
