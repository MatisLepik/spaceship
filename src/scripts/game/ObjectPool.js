/**
 * Object pooling is the practice of reusing objects we've already created, but are no longer using.
 * The point is to reduce memory management by reusing memory that we've already allocated.
 * For this implementation, all entities using ObjectPool must implement the activate
 * method, and indicate with `isUsed` whether the object is ready to be reused.
 */

export default class ObjectPool {

  constructor(Obj) {
    this.Obj = Obj;
    this.objects = [];
  }

  activate(...rest) {
    for (let i = 0; i < this.objects.length; i++) {
      const object = this.objects[i];

      if (!object.isUsed) {
        object.activate(...rest);
        return;
      }
    }

    this.objects.push(new this.Obj().activate(...rest));
  }

  getAll() {
    return this.objects;
  }
}
