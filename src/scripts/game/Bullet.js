import { isOutOfScreen, getCanvasContext } from 'scripts/game/stage';
import Entity from 'scripts/game/Entity';

const BULLET_SPEED = 1000;
const BULLET_COLOR = 'rgb(225,25,25)';

export default class Bullet extends Entity {

  activate(posX, posY, rotation) {
    return this.fire(posX, posY, rotation);
  }

  fire(posX, posY, rotation) {
    this.isUsed = true;
    this.posX = posX;
    this.posY = posY;
    this.rotation = rotation;
    this.setSpeedToRotation();

    return this;
  }

  draw() {
    const canvasContext = getCanvasContext();
    canvasContext.save();
    canvasContext.translate(this.posX, this.posY);
    canvasContext.rotate(-this.rotation);
    canvasContext.fillStyle = BULLET_COLOR;
    canvasContext.fillRect(0, 25, 2, 15);
    canvasContext.restore();
  }

  physx(delta) {
    this.posX += (this.speedX * delta);
    this.posY += (this.speedY * delta);

    if (isOutOfScreen(this)) {
      this.isUsed = false;
    }
  }

  setSpeedToRotation() {
    this.speedX = Math.sin(this.rotation) * BULLET_SPEED;
    this.speedY = Math.cos(this.rotation) * BULLET_SPEED;
  }
}
