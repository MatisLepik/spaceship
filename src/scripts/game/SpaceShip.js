import {
  getCanvasContext, getMousePos, getViewportDimensions, getObstacleFactory, getBulletFactory
} from 'scripts/game/stage';
import Entity from 'scripts/game/Entity';

const ANIMATION_TIME = 0.15;
const SPEED_MODIFIER = 2;
const FRICTION_MODIFIER = 6;
const SHIP_IMG = new Image();

SHIP_IMG.src = 'spaceship.png';

export default class SpaceShip extends Entity {

  constructor(posX, posY, radius) {
    super();
    this.isUsed = true;
    this.posX = posX;
    this.posY = posY;
    this.radius = radius;
    this.isMoving = false;
    this.sheetX = 0;
    this.lastAnimation = 0;
  }

  shootBullet() {
    getBulletFactory().activate(this.posX, this.posY, this.rotation);
  }

  draw() {
    const canvasContext = getCanvasContext();
    canvasContext.save();
    canvasContext.translate(this.posX, this.posY);
    canvasContext.rotate(-this.rotation);
    canvasContext.drawImage(SHIP_IMG, this.sheetX, 0, 50, 45, -25, -23, 50, 45);
    canvasContext.restore();
  }

  physx(delta) {
    const mousePos = getMousePos();

    this.detectCollision();
    this.animate(delta);
    this.setRotationToMousePos(mousePos);
    this.updateSpeed(delta, mousePos);

    this.posX += (this.speedX * delta);
    this.posY += (this.speedY * delta);
  }

  detectCollision() {
    const obstacles = getObstacleFactory().getAll();
    const viewportDimensions = getViewportDimensions();

    for (let i = 0; i < obstacles.length; i += 1) {
      const dX = this.posX - obstacles[i].posX + obstacles[i].radius;
      const dY = this.posY - obstacles[i].posY + obstacles[i].radius;
      const distance = Math.sqrt((dX * dX) + (dY * dY));

      if (this.radius + obstacles[i].radius > distance) {
        this.posX = viewportDimensions.width / 2;
        this.posY = viewportDimensions.height - 50;
        obstacles[i].isUsed = false;
        this.isMoving = false;
        return;
      }
    }
  }

  updateSpeed(delta, mousePos) {
    if (this.isMoving) {
      this.speedX = (mousePos.x - this.posX) * SPEED_MODIFIER;
      this.speedY = (mousePos.y - this.posY) * SPEED_MODIFIER;

    } else {
      this.speedX -= this.speedX * FRICTION_MODIFIER * delta;
      this.speedY -= this.speedY * FRICTION_MODIFIER * delta;
    }
  }

  animate(delta) {
    if (this.lastAnimation >= ANIMATION_TIME) {
      this.lastAnimation = 0;
      if (this.sheetX === 150) {
        this.sheetX = 0;
      } else {
        this.sheetX += 50;
      }
    }
    this.lastAnimation += delta;
  }

  setRotationToMousePos(mousePos) {
    this.rotation = Math.atan2(mousePos.x - this.posX, mousePos.y - this.posY);
  }
}
