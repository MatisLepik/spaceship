import { randomInt } from 'scripts/utils';
import SpaceShip from 'scripts/game/SpaceShip';
import ObjectPool from 'scripts/game/ObjectPool';
import Bullet from 'scripts/game/Bullet';
import Obstacle from 'scripts/game/Obstacle';
const bulletPool = new ObjectPool(Bullet);
const obstaclePool = new ObjectPool(Obstacle);

const canvas = document.getElementById('snow');
const canvasContext = canvas.getContext('2d');
const mousePos = {
  x: 0,
  y: 0
};

const obstacleFrequency = 50;

let isActive = false;
let lastTime;
let viewportWidth;
let viewportHeight;
let obstacleCreated;
let spaceShip;

export default function initGame() {
  updateSizes();
  spaceShip = new SpaceShip(viewportWidth / 2 - 25, viewportHeight / 2 - 25, 20);

  canvas.addEventListener('mousemove', updateMousePos);
  canvas.addEventListener('mousedown', onMouseDown);
  canvas.addEventListener('mouseup', onMouseUp);
  window.addEventListener('focus', updateLastTime);
}

export function stopGame() {
  canvas.removeEventListener('mousemove', updateMousePos);
  canvas.removeEventListener('mousedown', onMouseDown);
  canvas.removeEventListener('mouseup', onMouseUp);
  window.removeEventListener('focus', updateLastTime);
}

export function getCanvas() {
  return canvas;
}

export function getCanvasContext() {
  return canvasContext;
}

export function getMousePos() {
  return mousePos;
}

export function getViewportDimensions() {
  return {
    width: viewportWidth,
    height: viewportHeight
  };
}


export function isOutOfScreen(entity) {
  return entity.posX > viewportWidth || entity.posX < 0 || entity.posY < 0 || entity.posY > viewportHeight;
}

export function startLoop() {
  isActive = true;
  updateLastTime();
  obstacleCreated = new Date().getTime();
  loop();
}

export function stopLoop() {
  isActive = false;
}

export function getObstacleFactory() {
  return obstaclePool;
}

export function getBulletFactory() {
  return bulletPool;
}

function updateMousePos(evt) {
  mousePos.x = evt.pageX;
  mousePos.y = evt.pageY;
}

function updateSizes() {
  const $window = $(window);
  viewportWidth = $window.width();
  viewportHeight = $window.height();

  canvas.width = viewportWidth;
  canvas.height = viewportHeight;
}

function updateLastTime() {
  lastTime = new Date().getTime();
}

function onMouseUp(evt) {
  if (evt.which === 1) { // Left click
    spaceShip.isMoving = false;
  }
}

function onMouseDown(evt) {
  if (evt.which === 1) { // Left click
    spaceShip.isMoving = true;
  } else if (evt.which === 3) { // Right click
    spaceShip.shootBullet();
  }
}

function loop() {
  if (!isActive) return;
  canvasContext.clearRect(0, 0, canvas.width, canvas.height);

  const currentTime = (new Date()).getTime();
  const delta = (currentTime - lastTime) / 1000;

  createObstacleIfNeeded(currentTime);
  drawAndPhysics(delta);

  lastTime = currentTime;

  window.requestAnimationFrame(loop);
}

function drawAndPhysics(delta) {
  const bullets = bulletPool.getAll();
  const obstacles = obstaclePool.getAll();

  for (let i = 0; i < obstacles.length; i += 1) {
    obstacles[i].tick(delta);
  }

  for (let i = 0; i < bullets.length; i += 1) {
    bullets[i].tick(delta);
  }

  spaceShip.tick(delta);
}

function createObstacleIfNeeded(currentTime) {
  if (currentTime - obstacleCreated > obstacleFrequency) {
    obstaclePool.activate(randomInt(-100, viewportWidth), -10);
    obstacleCreated = currentTime;
  }
}
