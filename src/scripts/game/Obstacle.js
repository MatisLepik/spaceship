import { randomInt } from 'scripts/utils';
import { getCanvasContext, getViewportDimensions } from 'scripts/game/stage';
import Entity from 'scripts/game/Entity';

const SPEED_X = 10;
const SPEED_Y_BASE = 120;
const SPEED_Y_RADIUS_MODIFIER = 5;

export default class Obstacle extends Entity {

  activate(posX, posY) {
    return this.spawn(posX, posY);
  }

  spawn(posX, posY) {
    this.isUsed = true;
    this.radius = randomInt(1, 5);
    this.posX = posX;
    this.posY = posY;
    this.speedY = SPEED_Y_BASE + this.radius * SPEED_Y_RADIUS_MODIFIER;
    this.speedX = SPEED_X;
    this.color = this.getRandomColor();

    return this;
  }

  draw() {
    if (!this.isUsed) return;
    const canvasContext = getCanvasContext();

    canvasContext.fillStyle = this.color;
    canvasContext.beginPath();
    canvasContext.arc(this.posX, this.posY, this.radius, 0, 2 * Math.PI);
    canvasContext.fill();
  }

  physx(delta) {
    if (!this.isUsed) return;

    this.posX += (this.speedX * delta);
    this.posY += (this.speedY * delta);

    if (this.posY > getViewportDimensions().height) {
      this.isUsed = false;
    }
  }

  getRandomColor() {
    const randomColor = randomInt(180, 255);
    return `rgb(${randomColor},${randomColor},${randomColor})`;
  }
}
