### Spaceship ###

This is a really basic JavaScript "game" (if you can call it that). You can control a spaceship by holding down mouse1 and you have to dodge the white balls. Why are they white balls? I don't know, I was trying to see if I could simulate snowfall and then for some reason decided to put a spaceship there :D

**You can see the [live version](http://matislepik.eu/spaceship/) on my website.**

## Development ##

1. `npm install`
2. Develop: `gulp`, build for production: `gulp build-prod`

### Contact ###

- Matis Lepik

- matis.lepik@gmail.com

- www.matislepik.eu

![Screenshot](https://i.imgur.com/mZuJtOS.png)
