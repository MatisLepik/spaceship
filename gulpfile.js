const gulp = require('gulp');
const sass = require('gulp-sass');
const stripCssComments = require('gulp-strip-css-comments');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require('browserify');
const babelify = require('babelify');
const del = require('del');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const gutil = require('gulp-util');
const rev = require('gulp-rev');
const twig = require('gulp-twig');
const fs = require('fs');

/* Config */
const publicPath = 'public';
const paths = {
  style: {
    src: 'src/scss/style.scss',
    watch: 'src/scss/**/*.scss',
    dest: `${publicPath}/assets`
  },
  script: {
    src: 'src/scripts/main.js',
    watch: 'src/scripts/**/*.js',
    dest: `${publicPath}/assets`
  },
  copy: {
    src: ['src/**/*.*', '!src/twig/**/*.twig', '!src/scss/**/*.*', '!src/scripts/**/*.*'],
    dest: publicPath
  },
  twig: {
    src: ['src/twig/**/*.twig', '!src/twig/partials/**/*.twig'],
    watch: 'src/twig/**/*.twig',
    dest: publicPath
  },
  clean: [`${publicPath}/assets`],
  revManifest: '.'
};

/* Set environment variables */
process.env.NODE_PATH = 'src';

/* Helper tasks */

gulp.task('build', () => { // Basic build development version
  process.env.NODE_ENV = 'development';
  gulp.start('styles', 'scripts', 'twig', 'copy');
});

gulp.task('build-production', ['clean'], () => { // Production version deletes previous stuff, minifies, strips comments
  process.env.NODE_ENV = 'production';
  gulp.start('twig-after-rest');
});

gulp.task('twig-after-rest', ['styles-production', 'scripts-production', 'copy'], () => {
  gulp.start('twig-production');
});

gulp.task('default', ['build'], () => { // Default builds immediately and then starts watching
  gulp.start('serve');
});

gulp.task('serve', () => { // Watch for changes and browsersync

  browserSync.init({
    server: {
      baseDir: publicPath
    },
    notify: false
  });

  gulp.watch(paths.style.watch, ['styles']);
  gulp.watch(paths.script.watch, ['scripts']);
  gulp.watch(paths.copy.src, ['copy']);
  gulp.watch(paths.twig.watch, ['twig']);

  gulp.watch([`${publicPath}/**/*`]).on('change', browserSync.reload);
});

/* Development tasks */

gulp.task('scripts', () => {
  const b = browserify({
    entries: paths.script.src,
    debug: true
  }).transform(babelify, {
    presets: ['es2015']
  });

  return b.bundle()
    .on('error', function errHandler(err) {
      gutil.log(err);
      this.emit('end');
    })
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.script.dest))
    .pipe(rev.manifest({ merge: true, hash: 'f' }))
    .pipe(gulp.dest(paths.revManifest))
    .pipe(gulp.dest(paths.revManifest));
});

gulp.task('styles', () => gulp.src(paths.style.src)
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(autoprefixer('last 3 versions'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.style.dest))
    .pipe(rev.manifest({ merge: true, hash: 'f' }))
    .pipe(gulp.dest(paths.revManifest)));

gulp.task('twig', () => gulp.src(paths.twig.src)
    .pipe(twig({
      data: {
        paths: {
          'main.js': 'main.js',
          'style.css': 'style.css'
        }
      }
    }))
    .pipe(gulp.dest(paths.twig.dest)));

/* Production tasks */

gulp.task('twig-production', () => gulp.src(paths.twig.src)
    .pipe(twig({
      data: {
        paths: getPaths()
      }
    }))
    .pipe(gulp.dest(paths.twig.dest)));

gulp.task('scripts-production', () => {
  const b = browserify({
    entries: paths.script.src,
    debug: false
  }).transform(babelify, {
    presets: ['es2015']
  });

  return b.bundle()
    .on('error', function errHandler(err) {
      gutil.log(err);
      this.emit('end');
    })
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(rev())
    .pipe(gulp.dest(paths.script.dest))
    .pipe(rev.manifest({ merge: true }))
    .pipe(gulp.dest(paths.revManifest));
});

gulp.task('styles-production', () => gulp.src(paths.style.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(stripCssComments({
      all: true
    }))
    .pipe(autoprefixer('last 3 versions'))
    .pipe(cleanCSS())
    .pipe(rev())
    .pipe(gulp.dest(paths.style.dest))
    .pipe(rev.manifest({ merge: true }))
    .pipe(gulp.dest(paths.revManifest)));

gulp.task('clean', () => del(paths.clean));

/* General tasks (both dev and prod */

gulp.task('copy', () => gulp.src(paths.copy.src)
    .pipe(gulp.dest(paths.copy.dest)));

function getPaths() {
  const outputPaths = JSON.parse(fs.readFileSync(`${paths.revManifest}/rev-manifest.json`, 'utf8'));
  return outputPaths;
}
